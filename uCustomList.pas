{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      07-mai-2007													}
{       Version    2.0                                  }
{       Created by Project ThumbsView                   }
{*******************************************************}
unit uCustomList;

interface

uses System.SysUtils, System.Classes;


type
  TCustomList = class
  protected
    fList: TList;
    fSizeRec: Integer;
    fFunctionCompare: TListSortCompare;

    procedure AddInternal(P: Pointer); virtual;
    procedure	DeleteInternal(Index: LongWord; RemList: Boolean = True); virtual;
    function	CountInternal: Integer;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    function  AllocateItem: Pointer;
    procedure ReleaseItem(P: Pointer);
    procedure Delete(P: Pointer); overload; virtual;
    procedure Add(P: Pointer); overload; virtual;
    procedure Insert(Index: Integer; P: Pointer); virtual;

		function FindItem(Item: Pointer; var Index: Integer): Boolean; virtual;
    procedure	Sort; virtual;
    procedure Clear; virtual;
    function	Count: Integer; virtual;

    class function BinarySearch(List: TList; Item: Pointer; Compare: TListSortCompare; var Index: Integer): Boolean;
  end;

implementation


constructor TCustomList.Create;
begin
  inherited Create;
	fList := TList.Create;
end;

destructor TCustomList.Destroy;
begin
  if Assigned(fList) then
  begin
    Clear;
    FreeAndNil(fList);
  end;
  inherited;
end;

function TCustomList.FindItem(Item: Pointer; var Index: Integer): Boolean;
begin
	Result := BinarySearch(fList, Item, fFunctionCompare, Index);
end;

procedure TCustomList.Insert(Index: Integer; P: Pointer);
begin
	fList.Insert(Index, P);
end;

function TCustomList.AllocateItem: Pointer;
begin
	GetMem(Result, fSizeRec);
  FillChar(Result^, fSizeRec, 0);
end;

procedure TCustomList.ReleaseItem(P: Pointer);
begin
	FreeMem(P);
end;

procedure TCustomList.AddInternal(P: Pointer);
begin
  fList.Add(P);
end;

procedure TCustomList.Add(P: Pointer);
begin
	AddInternal(P);
end;

procedure TCustomList.Delete(P: Pointer);
var
		Index: Integer;
begin
  if Assigned(P) then
  begin
  	Index := fList.IndexOf(P);
    if (Index > -1)  and (Index < CountInternal) then
	    DeleteInternal(Index, True)
    else
    	raise Exception.Create('Error list index:' + IntToStr(Index) );
  end;

end;

procedure TCustomList.DeleteInternal(Index: LongWord; RemList: Boolean = True);
begin
  ReleaseItem(fList.Items[Index]);
  if RemList then
  	fList.Delete(Index);
end;

procedure TCustomList.Sort;
begin
	fList.Sort(fFunctionCompare);
end;

procedure TCustomList.Clear;
Var
		i: Integer;
begin
  for i := CountInternal-1 downto 0 do
    DeleteInternal(i, False);
  fList.Clear;
end;

function TCustomList.CountInternal: Integer;
begin
  Result := fList.Count;
end;

function TCustomList.Count: Integer;
begin
	Result := CountInternal;
end;

class function TCustomList.BinarySearch(List: TList; Item: pointer; Compare: TListSortCompare;
	var Index: Integer): Boolean;
var
		Last, First, Middle: Integer;
		CompareResult : Integer;
begin
	{������ �������� ��� �������� ������� � ���������� ���������}
	Last := 0;
	First := pred(List.Count);
  Index := 0;
	while (Last <= First) do
  begin
		{��������� ������ �������� ��������}
		Middle := (Last + First) div 2;
		{�������� �������� �������� �������� � ������� ���������}
		CompareResult := Compare(List.List[Middle], Item);
		{���� �������� �������� �������� ������ �������� ��������, ����������� ����� ������ �� ������� �� �������� �������}
		if (CompareResult < 0) then
			Last := succ(Middle)
		{���� �������� �������� �������� ������ �������� ��������, ����������� ������ ������ �� ������� ����� �������� �������}
		else if (CompareResult > 0) then
			First := pred(Middle)
		{� ��������� ������ ������� ������� ������}
		else
    begin
	    Index := Middle;
			Result := True;
      Exit;
		end;
	end;
  Index := Last;
	Result := False;
end;

end.

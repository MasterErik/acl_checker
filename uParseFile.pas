{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      19-mai-2014													}
{       Version    2.0                                  }
{       ����� ������� ACL �����  SVN										}
{       node                            	            	}
{       Created by Project ACL Checker                  }
{*******************************************************}
unit uParseFile;

interface

uses	System.StrUtils, System.SysUtils, System.Classes,
      ThreadBase,
      uNodeList, uGroupList, ConstACLChecker;

type
	RParseRec = record
  	Value: AnsiString;
    Size: Integer;
  end;

  TBlockType = (blkContinue, blkGroup, blkRepository);

	TACLParser = class
  private
		function ReadLine(var CurFile: TextFile): RParseRec;
    function DetectBlock(Line: RParseRec): TBlockType;
    procedure ParseGroup(Line: RParseRec);
    procedure ParseRepository(Line: RParseRec);
    function GetRepositoryName(Line: RParseRec): AnsiString;
  protected
    fOwner: TCustomThread;
		fProgress: Integer;
    fLineNr: Integer;
    fCountError: Integer;
    fFileName: string;
    fCurRepository: AnsiString;

    function GetTerminated: Boolean;
    procedure Error(Line: RParseRec);
    procedure ShowMessage(Value: string);
  public
  	constructor Create(Owner: TCustomThread; FileName: string);
    procedure ParseFile;
  end;


implementation

constructor TACLParser.Create(Owner: TCustomThread; FileName: string);
begin
  inherited Create;
  fFileName := FileName;
  fOwner := Owner;
end;

function TACLParser.GetRepositoryName(Line: RParseRec): AnsiString;
begin
	Result := Copy(Line.Value, 2, Line.Size-2);
end;

function TACLParser.ReadLine(var CurFile: TextFile): RParseRec;
begin
  repeat
    ReadLn(CurFile, Result.Value);
  until not ((Copy(Result.Value, 1, 1) = '#') or (Trim(Result.Value) = ''));
  Result.Value := Trim(Result.Value);
  Result.Size := Length(Result.Value);
end;

function TACLParser.DetectBlock(Line: RParseRec): TBlockType;
begin
	if (Line.Value = '[groups]') then
  	Result := blkGroup
  else if (Copy(Line.Value, 1, 1) = '[') and (Pos(':/', Line.Value) > 0) and (Copy(Line.Value, Line.Size) = ']') then
  	Result := blkRepository
  else
  	Result := blkContinue;
end;

procedure TACLParser.ParseGroup(Line: RParseRec);
var
    Index: Integer;
    i: Integer;
    corrctPos: Integer;
    Buf: AnsiString;
    Group, User: AnsiString;
begin
  Index := PosEx('=', Line.Value);
  if (Index = 0) then
  	Error(Line)
  else
  begin
    Group := Trim(Copy(Line.Value, 1, Index-1));
    Buf := Trim(Copy(Line.Value, Index+1));
    if (Buf = '') then
    	Error(Line);
    Index := 1;
    repeat
      i := PosEx(',', Buf, Index);
      if i = 0 then
      begin
        i := Length(Buf);
       	corrctPos := 0;
      end
      else
				corrctPos := Index;

      begin
        User := Trim(Copy(Buf, Index, i-corrctPos));
        GroupList.Add(Group, User);
      end;
      Index := i + 1;
    until (Length(Buf) <= Index);
  end;
end;

procedure TACLParser.ParseRepository(Line: RParseRec);
var
		UserName, Permissions: AnsiString;
    i: Integer;
begin
	i := PosEx('=', Line.Value);
  if i = 0 then
  	Error(Line)
  else
  begin
	  UserName := Trim(Copy(Line.Value, 1, i-1));
  	Permissions := Trim(Copy(Line.Value, i+1));
    NodeList.Add(UserName, fCurRepository, Permissions);
  end;
end;

procedure TACLParser.ParseFile;
var
    CurFile: TextFile;
    Line: RParseRec;

    CurrentBlock: TBlockType;
    CheckBlock: TBlockType;
begin
	AssignFile(CurFile, fFileName, 0);
	try
 	 	FileMode := 0;
	 	Reset(CurFile);
    fCountError := 0;
    fProgress := 0;
    fLineNr := 0;
    CurrentBlock := blkContinue;

    while not Eof(CurFile) and not GetTerminated do
    begin
      Line := ReadLine(CurFile);
      Inc(fProgress, Line.Size);
      Inc(fLineNr);

      CheckBlock := DetectBlock(Line);
      if (CheckBlock <> blkContinue) then
        CurrentBlock := CheckBlock;

      case CheckBlock of
        blkGroup: Continue;
        blkRepository:
          begin
            fCurRepository := GetRepositoryName(Line);
            Continue;
          end;
      end;

      case CurrentBlock of
        blkGroup: 			ParseGroup(Line);
        blkRepository: 	ParseRepository(Line);
      end;

    end;
  finally
    CloseFile(CurFile);
  end;

  if (fCountError > 0) then
	  ShowMessage('Parse file '+ fFileName + ' Error count:' + IntToStr(fCountError));
end;

procedure TACLParser.Error(Line: RParseRec);
begin
	ShowMessage(Format('Error line(%d): %s', [fLineNr, Line.Value]));
end;

function TACLParser.GetTerminated: Boolean;
begin
  if Assigned(fOwner) then
		Result := fOwner.GetTerminated
  else
  	Result := False;
end;

procedure TACLParser.ShowMessage(Value: string);
begin
  if Assigned(fOwner) then
		fOwner.ShowMessage(Value);
end;

end.

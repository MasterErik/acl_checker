{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      19-mai-2014							}
{       Version    2.0                                  }
{       Created by Project ACL Checker                  }
{*******************************************************}
unit uLoger;

interface
uses Windows, Messages, ThreadBase;

type
  TLoger = class
  protected
    fHandle: THandle;
    fCounter: Integer;
    function GetCounter: Integer;
  public
    constructor Create(Value: THandle);
    destructor Destroy; override;
    class procedure Show(Value: String);
    class procedure CreateEx(Value: THandle);
    property SHandle: THandle read fHandle;
    property Counter: Integer read GetCounter;
  end;


Var
  	Loger: TLoger = nil;
Const
  	PWM_EVENT = WM_USER + 300;

implementation
uses  SysUtils, ConstACLChecker;

{ TLoger }
constructor TLoger.Create(Value: THandle);
begin
  inherited Create;
  fHandle := Value;
end;

class procedure TLoger.CreateEx(Value: THandle);
begin
  if not Assigned(Loger) then
    Loger := TLoger.Create(Value);
end;

destructor TLoger.Destroy;
begin
  Loger := nil;
  inherited;
end;

function TLoger.GetCounter: Integer;
begin
	Result := fCounter;
  Inc(fCounter);
end;

class procedure TLoger.Show(Value: String);
begin
  if Assigned(Loger) then
  begin
  	if Loger.SHandle <> 0 then
    	PostMessage(Loger.SHandle, PWM_EVENT, 0, SetMsgStr(IntToStr(Loger.Counter)+' '+Value));
  end;
end;

end.

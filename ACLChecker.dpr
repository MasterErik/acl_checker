program ACLChecker;

uses
  Vcl.Forms,
  fMain in 'fMain.pas' {FrmMain},
  MsgBoxEx in 'MsgBoxEx.pas',
  uLoger in 'uLoger.pas',
  uLoadACL in 'uLoadACL.pas',
  uCustomList in 'uCustomList.pas',
  uParseFile in 'uParseFile.pas',
  uCustomSyncList in 'uCustomSyncList.pas',
  uGroupList in 'uGroupList.pas',
  uNodeList in 'uNodeList.pas',
  ThreadBase in 'ThreadBase.pas',
  ConstACLChecker in 'ConstACLChecker.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.

unit fMain;


interface

uses
  System.Classes, System.SysUtils, Winapi.Messages, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Forms,
  Vcl.ActnList, Vcl.ImgList,  Vcl.Controls, Vcl.Menus, Vcl.StdActns, Vcl.Dialogs, Datasnap.DBClient,
  Vcl.ToolWin, Vcl.Buttons, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Data.DB, System.Types, Vcl.Graphics,
  uLoadACL, uLoger, ConstACLChecker, Data.Bind.EngExt, Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs,
  Vcl.Bind.Editors, Data.Bind.Components;

type
  TCrackGrid = class (TDBGrid);

  TfrmMain = class(TForm)
    cdsStatictics: TClientDataSet;
    dsSource: TDataSource;
    dsClone: TDataSource;
    pnlGrid: TPanel;
    pnlUserDetail: TPanel;
    dbgDest: TDBGrid;
    pnlTitleLeft: TPanel;
    pnlRight: TPanel;
    pnlStatistics: TPanel;
    dbgSource: TDBGrid;
    pnlProgress: TPanel;
    pbProgress: TProgressBar;
    cdsRepository: TClientDataSet;
    Panel4: TPanel;
    ilMenu: TImageList;
    MainMenu: TMainMenu;
    miAndmed: TMenuItem;
    miQuit: TMenuItem;
    miAbi: TMenuItem;
    miCheckVersion: TMenuItem;
    N6: TMenuItem;
    miAbout: TMenuItem;
    tlbMenu: TToolBar;
    ToolButton: TToolButton;
    btnClose: TToolButton;
    N1: TMenuItem;
    OpenFile: TMenuItem;
    alMain: TActionList;
    acClose: TWindowClose;
    acMinimize: TWindowMinimizeAll;
    actFileOpen: TFileOpen;
    pnlLog: TPanel;
    pnlLogTitle: TPanel;
    mLog: TMemo;
    pnlFiles: TPanel;
    pnlFile: TPanel;
    lbFile: TLabel;
    edFileName: TEdit;
    butGetFileName: TBitBtn;
    actParseFile: TAction;
    ParseFile: TMenuItem;
    btnParseFile: TToolButton;
    btnShowGroups: TToolButton;
    btnShowRepository: TToolButton;
    actShowGroups: TAction;
    actShowRepository: TAction;
    btnClearLog: TToolButton;
    actClearLog: TAction;
    actShowRepTree: TAction;
    btnShowRepositoryTree: TToolButton;
    ToolButton1: TToolButton;
    BindingsList1: TBindingsList;
    procedure FormCreate(Sender: TObject);
    procedure dbgUserDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure miAboutClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actParseFileExecute(Sender: TObject);
    procedure actShowGroupsExecute(Sender: TObject);
    procedure actShowRepositoryExecute(Sender: TObject);
    procedure actClearLogExecute(Sender: TObject);
    procedure actShowRepTreeExecute(Sender: TObject);
    procedure actFileOpenAccept(Sender: TObject);
    procedure dbgTitleClick(Column: TColumn);
    procedure dbgSourceDblClick(Sender: TObject);
  private
    procedure ThreadWndProc(var msg: TMessage );  message PWM_EVENT;
    procedure ThreadProgress(var msg: TMessage ); message PWM_PROGRESS;

    procedure StartProgress(Value: Integer);
    procedure ShowProgress(Value: integer);
    procedure EndProgress;
    procedure CreateThread;
    procedure DestroyThread;
    procedure CreateStatistics;
    procedure AppendUserStat(SumRec: RStatic);
    procedure CreateRepository;
    procedure FillRepository(UserName: String);
  public
    procedure HandleException(Sender: TObject; E: Exception);
  end;

var
  frmMain: TfrmMain;

implementation
uses ThreadBase, uNodeList, uGroupList;

{$R *.DFM}

function SortClientDataSet(ClientDataSet: TClientDataSet; const FieldName: String): Boolean;
var
  i: Integer;
  NewIndexName: String;
  IndexOptions: TIndexOptions;
  Field: TField;
begin
  Result := False;
  Field := ClientDataSet.Fields.FindField(FieldName);
  //If invalid field name, exit.
  if Field = nil then Exit;
  //if invalid field type, exit.
  if (Field is TObjectField) or (Field is TBlobField) or (Field is TAggregateField)
  	or (Field is TVariantField) or (Field is TBinaryField) then
  	Exit;
  //Get IndexDefs and IndexName using RTTI
  //Ensure IndexDefs is up-to-date
  ClientDataSet.IndexDefs.Update;
  //If an ascending index is already in use,
  //switch to a descending index
  if ClientDataSet.IndexName = FieldName + '__IdxA' then
  begin
    NewIndexName := FieldName + '__IdxD';
    IndexOptions := [ixDescending];
  end
  else
  begin
    NewIndexName := FieldName + '__IdxA';
    IndexOptions := [];
  end;
  //Look for existing index
  for i := 0 to Pred(ClientDataSet.IndexDefs.Count) do
  begin
    if ClientDataSet.IndexDefs[i].Name = NewIndexName then
    begin
      Result := True;
      Break
    end;
  end;
  //If existing index not found, create one
  if not Result then
  begin
    ClientDataSet.AddIndex(NewIndexName, FieldName, IndexOptions);
    Result := True;
  end;
  //Set the index
  ClientDataSet.IndexName := NewIndexName;
end;

procedure TfrmMain.ThreadWndProc(var msg: TMessage);
begin
  if mLog.Lines.Count > 32000 then
    mLog.Lines.Clear;
  mLog.Lines.Add(GetMsgStr(msg.LParam));
  pnlLog.Visible := True;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	DestroyThread;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FormatSettings.DecimalSeparator := '.';
  FormatSettings.DateSeparator := '.';
  FormatSettings.ShortDateFormat := 'dd.mm.yyyy';
  Application.OnException := HandleException;
  CreateThread;
end;

//�������� ���� ����������� ������� � ������� ��� ����������. ��� ��������
// ������ ��������� � ����� ������� � �������� ����������� ����������.
procedure TfrmMain.CreateThread;
begin
  IsMultiThread := True;
  TLoger.CreateEx(Handle);
  if not Assigned(GroupList) then
    GroupList := TGroupSyncList.Create;
  if not Assigned(NodeList) then
    NodeList := TNodeSyncList.Create;
  if not Assigned(LoadACL) then
		LoadACL := TLoadACL.Create(Handle);
end;

//��������� � ������������ ������� � ������.
procedure TfrmMain.DestroyThread;
begin
  ThreadExit(LoadACL);
  LoadACL := nil;
  if Assigned(NodeList) then
	  FreeAndNil(NodeList);
  if Assigned(GroupList) then
	 	FreeAndNil(GroupList);
  if Assigned(Loger) then
  	FreeAndNil(Loger);
end;


procedure TfrmMain.HandleException(Sender: TObject; E: Exception);
begin
  TLoger.Show(E.Message+' '+IntToStr(E.HelpContext));
end;

procedure TfrmMain.miAboutClick(Sender: TObject);
begin
	ShowMessage('��� ��������� ��� �������� ���� SVN �������������.');
end;

procedure TfrmMain.ThreadProgress(var msg: TMessage);
var
    Info: PInfo;
begin
  Info := PInfo(msg.LParam);

  case Info.Tyyp of
    prStart:      StartProgress(Info.Value);
    prProgress:   ShowProgress(Info.Value);
    prEnd:        EndProgress;
  end;
  Dispose(Info);
end;

procedure ShowRepository;
var
  i: Integer;
  j: Integer;
begin
	LoadACL.WaitExecute;
  for i := 0 to NodeList.Count-1 do
    with NodeList.Items[i]^ do
    begin
      TLoger.Show(Format('Repository: %s size: %d', [repository, size]));
      for j := 0 to size-1 do
				TLoger.Show(Format('    user:%s = %s', [users[j].name, users[j].permission]));
    end;
end;

procedure ShowRepositoryTree;
var
  i: Integer;
begin
	LoadACL.WaitExecute;
  for i := 0 to NodeList.Count-1 do
    with NodeList.Items[i]^ do
      TLoger.Show(Format('base:%2d Level:%2d parentIdx:%2d Repository: %s', [i, level, parent, repository]));
end;


procedure ShowGroups;
var
	i: Integer;
	j: Integer;
begin
	LoadACL.WaitExecute;
  for I := 0 to GroupList.Count-1 do
    with GroupList.Items[I]^ do
    begin
      TLoger.Show(Format('Group: %s', [group]));
      for j := 0 to size-1 do
				TLoger.Show(Format('    user: %s', [users[j].name ]));
    end;
end;

procedure TfrmMain.actClearLogExecute(Sender: TObject);
begin
	mLog.Clear;
end;

procedure TfrmMain.actFileOpenAccept(Sender: TObject);
begin
	edFileName.Text := TFileOpen(Sender).Dialog.FileName;
	LoadACL.StartParse(edFileName.Text);
end;

procedure TfrmMain.actParseFileExecute(Sender: TObject);
begin
	LoadACL.StartParse(edFileName.Text);
end;

procedure TfrmMain.actShowGroupsExecute(Sender: TObject);
begin
	ShowGroups;
end;

procedure TfrmMain.actShowRepositoryExecute(Sender: TObject);
begin
	ShowRepository;
end;

procedure TfrmMain.actShowRepTreeExecute(Sender: TObject);
begin
	ShowRepositoryTree;
end;

procedure TfrmMain.dbgUserDrawColumnCell(Sender: TObject;  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if TCrackGrid(Sender).DataLink.DataSet.FieldByName('Pr').AsInteger > 74 then
  begin
		TDBGrid(Sender).Canvas.Brush.Color := clWhite;
    TDBGrid(Sender).Canvas.Font.Color := clRed;
  end
  else if TCrackGrid(Sender).DataLink.DataSet.FieldByName('Pr').AsInteger < 5 then
  begin
		TDBGrid(Sender).Canvas.Brush.Color := clWhite;
    TDBGrid(Sender).Canvas.Font.Color := clBlue;
  end;

  TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
//clGreen;
end;

procedure TfrmMain.dbgSourceDblClick(Sender: TObject);
begin
	FillRepository(TDBGrid(Sender).DataSource.DataSet.FieldByName('UserName').AsString);
end;

procedure TfrmMain.dbgTitleClick(Column: TColumn);
begin
  SortClientDataSet(TClientDataSet(Column.Grid.DataSource.DataSet), Column.FieldName);
end;

procedure TfrmMain.StartProgress(Value: Integer);
begin
	pbProgress.Max := Value;
  pnlProgress.Visible := True;
  Application.ProcessMessages;
end;

procedure TfrmMain.ShowProgress(Value: integer);
begin
	pbProgress.Position := Value;
  Application.ProcessMessages;
end;

procedure TFrmMain.EndProgress;
begin
  pbProgress.Max := pbProgress.Position;
//  pnlProgress.Visible := False;
	CreateStatistics;
  CreateRepository;
end;

procedure TFrmMain.CreateRepository;
begin
  cdsRepository.Close;
	cdsRepository.CreateDataSet;
end;

procedure TFrmMain.CreateStatistics;
var
		Size: Integer;
  	i: Integer;
    CountRepository: Integer;
    SumRec: RStatic;
begin
  SumRec.UserName := '';
  SumRec.RefCount := 0;
  SumRec.CountRep := NodeList.Count;
  cdsStatictics.Close;
  cdsStatictics.CreateDataSet;

  cdsStatictics.DisableControls;
  try
    Size := NodeList.UserList.Count-1;
    for i := 0 to Size do
    begin
      if (SumRec.UserName = NodeList.UserList[i].name) then
        Inc(SumRec.RefCount)
      else
      begin
        if (SumRec.UserName <> '') and (SumRec.RefCount > 0)then
          AppendUserStat(SumRec);
        SumRec.UserName := NodeList.UserList[i].name;
        SumRec.RefCount := 1;
      end;
    end;
    if (SumRec.UserName <> '') and (SumRec.RefCount > 0)then
      AppendUserStat(SumRec);
	finally
		cdsStatictics.EnableControls;
  end;

end;

procedure TFrmMain.AppendUserStat(SumRec: RStatic);
begin
 	with cdsStatictics do
  begin
    Append;
    FieldByName('UserName').AsString := SumRec.UserName;
    FieldByName('RefCount').AsInteger := SumRec.RefCount;
    FieldByName('Pr').AsInteger := Trunc( (SumRec.RefCount / SumRec.CountRep) * 100 );
    Post;
	end;
end;

procedure TFrmMain.FillRepository(UserName: String);
var
		Size: Integer;
    i: Integer;

begin
  Size := NodeList.Count-1;

  with cdsRepository do
  begin
		DisableControls;
    EmptyDataSet;
    for i := 0 to Size do
    begin
      if NodeList.UserExists(NodeList[i], UserName) then
      begin
        Append;
        FieldByName('Repository').AsString := NodeList[i].repository;
        FieldByName('Level').AsInteger := NodeList[i].level;
        FieldByName('Users').AsInteger := NodeList[i].size;
        Post;
      end;

    end;
		EnableControls;
  end;
end;

end.

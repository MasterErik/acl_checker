{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      26-mai-2014													}
{       Version    1.0                                  }
{       Created by Project ThumbsView                   }
{*******************************************************}
unit ConstACLChecker;

interface
uses Messages, Sysutils, Windows;

type
	TProgressType = (prStart, prProgress, prEnd);

  RInfo = record
    Value: Integer;
    Tyyp: TProgressType;
  end;
  PInfo = ^RInfo;

  RStatic = record
  	UserName: string;
    RefCount: Integer;
    CountRep: Integer;
  end;

	function GetMsgStr(Value: Integer): String;
	function SetMsgStr(Value: String): Integer;
	function GetModuleFileName(PHandle: LongWord = 0): string;

const
  PWM_PROGRESS = WM_USER + 200 + 1;


implementation

function GetMsgStr(Value: Integer): String;
var
  P: PChar;
begin
  P := PChar(Value);
  Result := StrPas(P);
  FreeMem(P);
end;

function SetMsgStr(Value: String): Integer;
var
  P: PChar;
begin
  GetMem(P, Length(Value) * SizeOf(Char) + 1 * SizeOf(Char));
  StrCopy(P, PChar(Value));
  Result := Integer(P);
end;

function GetModuleFileName(PHandle: LongWord = 0): string;
var
  Buffer: array[0..261] of Char;
begin
  if PHandle = 0 then
    PHandle := HInstance;
  SetString(Result, Buffer, Windows.GetModuleFileName(PHandle, Buffer, SizeOf(Buffer)));
end;

end.

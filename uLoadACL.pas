{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      29-mai-2014													}
{       Version    2.0                                  }
{       ����� ������� �������� ����� � ������	���				}
{				������������� ����������� ������ ���� � ����		}
{       ������ TNodeSyncList.														}
{       Created by Project AclChecker                   }
{*******************************************************}
unit uLoadACL;

interface
uses System.Classes, System.SysUtils, Windows,
		ThreadBase, uNodeList, uGroupList, ConstACLChecker;

type
  EReadFile = class(Exception);

  TLoadACL = class(TCustomThread)
  private
    fFileName: string;
    fHandle: THandle;
    fExitEvent: Boolean;
    fKey: Cardinal;

    procedure CopyUsersRights(ParentIdx, Size: Integer);
    procedure PrepareNodes;
    procedure BuildTree(const ParentIdx, Last, BLevel: Integer; const ParentRep: AnsiString);
    procedure ExpandUsersRecursive;
    procedure ReplaceGroups;
    procedure ReplaceAllUsers(Item: PNodeItem; Index: Integer);
    procedure ReplaceGroupToUsers(Item: PNodeItem; Index: Integer);
  protected
    procedure SendProgress(Value: integer; Tyyp: TProgressType);
    procedure InternalExec(Sender: TObject); override;
    procedure InternalException(const Status: Cardinal); override;
  public
    constructor	Create(Handle: THandle); reintroduce;

    procedure StartParse(NewFileName: string);
    procedure ShowMessage(Value: String); override;
		function	GetTerminated: Boolean; override;

    procedure StartProgress;
    procedure Progress(Position: integer);
    procedure EndProgress;

    property  FileName: String read fFileName;
  end;

var
  	LoadACL: TLoadACL;

implementation
uses uLoger, uParseFile, Math;

{ TLoadACL }
constructor TLoadACL.Create(Handle: THandle);
begin
  inherited Create;
  FHandle := Handle;
end;

procedure TLoadACL.SendProgress(Value: integer; Tyyp: TProgressType);
var
    pRecInfo: PInfo;
begin
  if FHandle <> 0 then
  begin
    New(pRecInfo);
    pRecInfo.Tyyp := Tyyp;
    pRecInfo.Value := Value;
  	PostMessage(FHandle, PWM_PROGRESS, 0, Integer(pRecInfo));
  end;
end;

procedure TLoadACL.StartProgress;
begin
  SendProgress(4, prStart);
end;

procedure TLoadACL.Progress(Position: integer);
Const
			LastTime: Cardinal = 0;
begin
  if (GetTickCount - LastTime > 70) then
  begin
	  SendProgress(Position, prProgress);
    LastTime := GetTickCount;
  end;
end;

procedure TLoadACL.EndProgress;
begin
  SendProgress(0, prEnd);
end;

function TLoadACL.GetTerminated: Boolean;
begin
	Result := inherited or fExitEvent;
end;

procedure TLoadACL.ShowMessage(Value: String);
begin
	TLoger.Show(Value);
end;

procedure TLoadACL.InternalException(const Status: Cardinal);
begin
	if Terminated then
    exit;
  inherited;
  ShowMessage(fLastException.Message);
end;

//���������� ������ Adjacency List, ������� ������������ ������� �� ������
procedure TLoadACL.BuildTree(const ParentIdx, Last, BLevel: Integer; const ParentRep: AnsiString);
var
		i: Cardinal;
begin
	if fExitEvent then
  	exit;

	for i := ParentIdx + 1 to Last-1 do
  begin
    if (Copy(NodeList[i].repository, 1, Length(ParentRep)) = ParentRep) and (NodeList[i].level = 0) then
    begin
      NodeList[i].parent := ParentIdx;
      NodeList[i].level := BLevel;
      BuildTree(i, Last, BLevel + 1, NodeList[i].repository);
    end;
  end;
end;

procedure TLoadACL.PrepareNodes;
var
    Size: Integer;
    ParentIdx: Integer;
    Level: Cardinal;
begin
  Size := NodeList.Count;
	if Size > 0 then
  begin
    fKey := 0;
    NodeList.LockW;
    try
	    ParentIdx := -1;
      Level := 1;
	    BuildTree(ParentIdx, Size, Level, '');
    finally
			NodeList.UnLockW;
    end;
  end;

//	Progress(i);
//  EndProgress;
end;

procedure TLoadACL.CopyUsersRights(ParentIdx, Size: Integer);
var
		i: Integer;
    j: Integer;
    Parent: PNodeItem;
    Child: PNodeItem;
    User: AnsiString;
    Permission: String[2];
begin
	for i := ParentIdx to Size do
		if (NodeList[i].parent = ParentIdx) then
    begin
      Parent := NodeList[ParentIdx];
      Child := NodeList[i];
      //�� �������� ������������� � �������� ����  ���� � ��������� ����������� ����� '*'
      if not NodeList.UserExists(Child, '*') then
	      for j := 0 to Parent.size-1 do
        begin
        	User := Parent.users[j].name;
          Permission := Parent.users[j].permission;
          //�� �������� ������������ '*' ������������� ���� ���� � ��� ��� ����
          if not ( (Copy(User, 1, 1) = '*') and (Permission = '') ) then
	  	      NodeList.AddUser(Child, User, Permission);
        end;
      CopyUsersRights(i, Size);
    end;
end;

procedure TLoadACL.ExpandUsersRecursive;
var
		i: Integer;
    Size: Integer;
begin
	Size := NodeList.Count-1;
  NodeList.LockW;
  try
    for i := 0 to Size do
      if (NodeList[i].level = 1) then
        CopyUsersRights(i, Size);
  finally
		NodeList.UnLockW;
  end;
end;

procedure TLoadACL.ReplaceAllUsers(Item: PNodeItem; Index: Integer);
begin
  if Item.users[Index].permission = '' then
	  NodeList.DeleteUser(Item, Index)
  else
  begin
    raise EReadFile.Create('Error: * = ' + string(Item.users[Index].permission) + '  the list of users isn"t existed');
  end;
end;

procedure TLoadACL.ReplaceGroupToUsers(Item: PNodeItem; Index: Integer);
var
		Group: PGroup;
    i: Integer;
    User: AnsiString;
    Permission: String[2];
begin
  Group := GroupList.AllocateItem;
  try
    User := Copy(Item.users[Index].name, 2);
    Permission := Item.users[Index].permission;
    Group.group := User;

		NodeList.DeleteUser(Item, Index);

    if GroupList.FindItem(Group, Index) then
      for i := 0 to GroupList[Index].size-1 do
        NodeList.AddUser(Item, GroupList[Index].users[i].name, Permission)
    else
      raise EReadFile.Create('Error: ' + string(User) + ' group is not found');

  finally
		GroupList.ReleaseItem(Group);
  end;
end;

procedure TLoadACL.ReplaceGroups;
var
		i: Integer;
    j: Integer;
    Size: Integer;
begin
	Size := NodeList.Count-1;
  NodeList.LockW;
  try
    for i := 0 to Size do
    begin
    	for j := NodeList[i].size - 1 downto 0  do
      	with NodeList[i].users[j]^ do
        begin
					case NodeList[i].users[j].name[1] of
          	'*': ReplaceAllUsers(NodeList[i], j);
            '@': ReplaceGroupToUsers(NodeList[i], j);
          end;
        end;

      //��������� ���� ��� �������� ������������� � ������� ������� �� ������
      //��� ������ ���� ������� ����������, ����� ��� ������������ �� ����� �������� ����� ����� ������
      for j := NodeList[i].size - 1 downto 0 do
      	with NodeList[i].users[j]^ do
        	if NodeList[i].users[j].permission = '' then
				    NodeList.DeleteUser(NodeList[i], j);
    end;
  finally
		NodeList.UnLockW;
  end;
end;


procedure TLoadACL.InternalExec(Sender: TObject);
var
		Parser: TACLParser;
begin
	StartProgress;
  NodeList.Clear;
  GroupList.Clear;
  Parser := TACLParser.Create(Self, FileName);
  try
	  Parser.ParseFile;
  finally
    Parser.Free;
  end;
  Progress(1);
  PrepareNodes;
  Progress(2);
  ExpandUsersRecursive;
  Progress(3);
  ReplaceGroups;
  Progress(4);
  EndProgress;
  inherited;
end;

procedure TLoadACL.StartParse(NewFileName: string);
begin
  fExitEvent := True;
  WaitExecute;
  fFileName := NewFileName;
  fExitEvent := False;
  Start;
end;

end.

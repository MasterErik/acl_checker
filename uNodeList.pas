{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      19-mai-2014													}
{       Version    2.0                                  }
{       ����� ���������������� ������ ��� ��������			}
{       node                            	            	}
{       Created by Project ACL Checker                  }
{*******************************************************}
unit uNodeList;

interface

uses System.SysUtils, System.Classes,
		 uCustomSyncList, uCustomList;

type
  EUserListExceprion = class(Exception);

  PNodeItem = ^RNodeItem;

  RUser = Record
    name: AnsiString;    //���� ���������� �� @ �� ��� ������
    permission: string[2];
  end;
  PUser = ^RUser;

  TUserList = array[0..Maxint div 16 - 1] of PUser;
  PUserList = ^TUserList;

  RNodeItem = record
    repository: AnsiString;
    users: PUserList;
    size: Integer;
    //���� ������� ���� ����� ��� ����������� ������ ������� Adjacency List
    level: Cardinal;	//������� � ������ ������������, ���� ��� ����������� ���������� �������
    parent:  Integer;	//������ ��������
  end;


  TUsersList = class(TCustomSyncList)
  private
    function	GetItem(Index: Integer): PUser;
  public
    constructor Create; override;
    function	Add(User, Permissions: AnsiString): PUser; overload;

    property	Items[Index: Integer]: PUser read GetItem; default;
  end;

  TNodeSyncList = class(TCustomSyncList)
  private
    fUserList: TUsersList;
    function	GetItem(Index: Integer): PNodeItem;
    procedure ReSizeUserList(Item: PNodeItem);
  protected
    procedure	DeleteInternal(Index: LongWord; RemList: Boolean = True); override;
  public
    fCounter: Integer;
    constructor Create; override;
    destructor Destroy; override;

    procedure Add(User, Repository, Permissions: AnsiString); overload;
    procedure AddUser(Item: PNodeItem; User, Permissions: AnsiString);
    procedure DeleteUser(Item: PNodeItem; Index: Integer);
    function	UserExists(Item: PNodeItem; User: AnsiString): Boolean;

    property	Items[Index: Integer]: PNodeItem read GetItem; default;
    property	UserList: TUsersList read fUserList;
  end;

Var
		NodeList: TNodeSyncList = nil;

implementation

Uses uLoger;

function InternalCompare(Item1, Item2: Pointer): Integer;
begin
  Result := AnsiCompareStr(string(PNodeItem(Item1).repository), string(PNodeItem(Item2).repository));
end;


function InternalCompareUsersList(Item1, Item2: Pointer): Integer;
begin
  Result := AnsiCompareStr(string(PUser(Item1).name), string(PUser(Item2).name));
end;

{ TUsersList }
constructor TUsersList.Create;
begin
  inherited;
  fSizeRec := SizeOf(RUser);
  fFunctionCompare := InternalCompareUsersList;
end;

function TUsersList.Add(User, Permissions: AnsiString): PUser;
var
		Item: PUser;
    Index: Integer;
begin
	Item := AllocateItem;
  Item.name := User;
  Item.permission := Permissions;
  FindItem(Item, Index);
  Insert(Index, Item);
  Result := Item;
end;


function TUsersList.GetItem(Index: Integer): PUser;
begin
	Result := PUser(fList[Index]);
end;

{ TNodeSyncList }
constructor TNodeSyncList.Create;
begin
  inherited;
  fUserList := TUsersList.Create;
  fSizeRec := SizeOf(RNodeItem);
  fFunctionCompare := InternalCompare;
end;

destructor TNodeSyncList.Destroy;
begin
  inherited;
  if Assigned(fUserList) then
  	fUserList.Free;
end;

procedure TNodeSyncList.DeleteInternal(Index: LongWord; RemList: Boolean);
var
		Item: PNodeItem;
	  i: Integer;
begin
	Item := Items[Index];
  fUserList.LockW;
  try
	  for i := Item.size-1 downto 0 do
  	  fUserList.DeleteInternal(fUserList.fList.IndexOf(Item.users[i]));
  finally
		fUserList.UnLockW;
  end;
	Item.size := 0;
  ReSizeUserList(Item);
  inherited;
end;

procedure TNodeSyncList.ReSizeUserList(Item: PNodeItem);
var
		Size: Integer;
begin
	Size := Item.size;
	if (Size mod 5 <> 0) then
    Size := Trunc(Size / 5) * 5 + 5;
  ReallocMem(Item.users, Size * SizeOf(Item.users));
end;

procedure TNodeSyncList.DeleteUser(Item: PNodeItem; Index: Integer);
var
		Size, i: Integer;
begin
  Size := Item.size;
  fUserList.Delete(Item.users[Index]);
  Dec(Item.size);
	if Size - 1 > Index then
  	for i := Index to Size - 1 do
    	Item.users[i] := Item.users[i + 1];
//  if Size - 1 > Index then
//		Move(Item.users[Index+1], Item.users[Index], SizeOf(Item.users)*Item.size);
 	ReSizeUserList(Item);
end;

function TNodeSyncList.UserExists(Item: PNodeItem; User: AnsiString): Boolean;
var
		i: Integer;
begin
	Result := False;
	for i := 0 to Item.size-1 do
    if Item.users[i].name = User then
    begin
      Result := True;
      Break;
    end;
end;

procedure TNodeSyncList.AddUser(Item: PNodeItem; User, Permissions: AnsiString);
begin
	if not UserExists(Item, User) then
    with Item^ do
    begin
      Inc(size);
      ReSizeUserList(Item);
      users^[size-1] := fUserList.Add(User, Permissions);
    end
end;

procedure TNodeSyncList.Add(User, Repository, Permissions: AnsiString);
var
		Item: PNodeItem;
    Index: Integer;
    Found: Boolean;
begin
	Item := AllocateItem;
  try
	  Item.repository := Repository;
	  Found := FindItem(Item, Index);
  finally
  	ReleaseItem(Item);
  end;

  if Found then
  begin
  	LockW;
    try
	    Item := Items[Index];
      AddUser(Item, User, Permissions);
    finally
    	UnlockW;
    end;
  end
  else
  begin
    Item := AllocateItem;
    Item.repository := Repository;
    AddUser(Item, User, Permissions);
    Insert(Index, Item);
  end;

end;

function TNodeSyncList.GetItem(Index: Integer): PNodeItem;
begin
  Result := PNodeItem(fList[Index]);
end;


end.

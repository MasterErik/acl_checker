{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      20-mai-2014													}
{       Version    1.0                                  }
{       ����� ���������������� ������ ��� ��������			}
{       ����� � �������������            	            	}
{       Created by Project ACL Checker                  }
{*******************************************************}
unit uGroupList;

interface

uses System.SysUtils, System.Classes,
		 uCustomSyncList;

type
  RUserOf = Record
    name: AnsiString;
  end;
  PUserOf = ^RUserOf;

  TUserList = array[0..Maxint div 16 - 1] of PUserOf;
  PUserList = ^TUserList;


  RGroup = Record
    group: AnsiString;
    users: PUserList;
    size: Integer;
  end;
  PGroup = ^RGroup;

	TGroupSyncList = class(TCustomSyncList)
  private
    procedure ReSizeUserList(Item: PGroup);
    procedure AddUser(Item: PGroup; User: AnsiString);
  protected
    function	GetItem(Index: Integer): PGroup;
    procedure DeleteInternal(Index: LongWord; RemList: Boolean = True); override;
  public
    constructor Create; override;
    procedure Add(Group, User: AnsiString); overload;

    property	Items[Index: Integer]: PGroup read GetItem; default;
  end;

Var
		GroupList: TGroupSyncList = nil;

implementation

function InternalCompare(Item1, Item2: Pointer): Integer;
begin
  Result := AnsiCompareStr(PGroup(Item1).group, PGroup(Item2).group);
end;

{ TGroupSyncList }
constructor TGroupSyncList.Create;
begin
  inherited;
  fSizeRec := SizeOf(RGroup);
  fFunctionCompare := InternalCompare;
end;

procedure TGroupSyncList.ReSizeUserList(Item: PGroup);
var
		Size: Integer;
begin
	Size := Item.size;
	if (Size mod 5 <> 0) then
    Size := Trunc(Size / 5) * 5 + 5;
  ReallocMem(Item.users, Size * SizeOf(Item.users));
end;

procedure TGroupSyncList.AddUser(Item: PGroup; User: AnsiString);
var
		PUser: PUserOf;
begin
  with Item^ do
  begin
    Inc(size);
    ReSizeUserList(Item);
    New(PUser);
    PUser.name := User;
    users^[size-1] := PUser;
  end
end;

procedure TGroupSyncList.Add(Group, User: AnsiString);
var
		Item: PGroup;
    Found: Boolean;
    Index: Integer;
    PUser: PUserOf;
begin
  Item := AllocateItem;
  try
    Item.group := Group;
    Found := FindItem(Item, Index);
  finally
    ReleaseItem(Item);
  end;

  if Found then
  begin
  	LockW;
    try
	    Item := Items[Index];
      AddUser(Item, User);
    finally
    	UnlockW;
    end;
  end
  else
  begin
    Item := AllocateItem;
    Item.group := Group;
    AddUser(Item, User);
    Insert(Index, Item);
  end;
end;

procedure TGroupSyncList.DeleteInternal(Index: LongWord; RemList: Boolean = True);
var
		i: Integer;
    Item: PGroup;
begin
  Item := Items[Index];
  for i := Item.size-1 downto 0 do
    Dispose(Item.users[i]);
  Item.size := 0;
  ReSizeUserList(Item);
	inherited;
end;


function TGroupSyncList.GetItem(Index: Integer): PGroup;
begin
  Result := PGroup(fList[Index]);
end;

end.

{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      07-mai-2004													}
{       Version    1.0                                  }
{       Created by Project ThumbsView                   }
{*******************************************************}
unit MsgBoxEx;

interface

uses
  Windows, UITypes, Forms;

function MessageBox(Title, Text: string; Icon, Buttons: Longint; Default: Byte =
  0): TModalResult;
procedure MessageBoxS(Text: string);

implementation


var
  hHook: THandle = 0;

procedure ChangeButtonCaption(Dialog: HWnd; ControlID: Integer; const Caption:
  string);
begin
  SetWindowText(GetDlgItem(Dialog, ControlID), PChar(Caption))
end;

function ChangeCaptionsProc(nCode: Integer; wParam: WPARAM; lParam: LPARAM):
  LRESULT; stdcall;
begin
  Result := CallNextHookEx(hHook, nCode, wParam, lParam);
  if nCode = HCBT_ACTIVATE then
  begin
    ChangeButtonCaption(wParam, IDYES, '&���');
    ChangeButtonCaption(wParam, IDNO, '&��');
    ChangeButtonCaption(wParam, IDCANCEL, '&��������');
    ChangeButtonCaption(wParam, IDAbort, '&��������');
//    ChangeButtonCaption(wParam, IDOK, 'Ok');
    ChangeButtonCaption(wParam, IDIGNORE, '&������������');
    ChangeButtonCaption(wParam, IDRETRY, '&������');
    UnhookWindowsHookEx(hHook)
  end
end;

function MessageBoxLng(Parent: HWnd; MsgTitle, MsgText: string; Flags: Longint):
  Integer;
begin
  hHook := SetWindowsHookEx(WH_CBT, @ChangeCaptionsProc, hInstance,
    GetCurrentThreadID);
  Result := Windows.MessageBoxEx(Parent, PChar(MsgText), PChar(MsgTitle),
    Flags, LANG_RUSSIAN);
end;

function MessageBoxEx(ParentForm: TCustomForm; MsgTitle, MsgText: string;
  MsgIcon, Buttons: Longint; DefaultButton: Byte = 0): TModalResult;
var
  Flags: Longint;
  Parent: THandle;
begin
  if Assigned(ParentForm) then
    Parent := ParentForm.Handle
  else
    Parent := Application.Handle;

  Flags := Buttons + MsgIcon + mb_TaskModal + ((DefaultButton and 7) shl 8);
  Result := MessageBoxLng(Parent, MsgTitle, MsgText, Flags);
end;

function MessageBox(Title, Text: string; Icon, Buttons: Longint; Default: Byte =
  0): TModalResult;
begin
  Result := MessageBoxEx(Screen.ActiveCustomForm, Title, Text, Icon, Buttons,
    Default);
end;

procedure MessageBoxS(Text: string);
begin
  MessageBox(Application.Title, Text, MB_ICONINFORMATION, MB_OK);
end;

end.

unit uCustomSyncList;
{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      07-mai-2007													}
{       Version    2.0                                  }
{       Created by Project ThumbsView                   }
{*******************************************************}

interface

uses System.SysUtils, System.Classes,
		 uCustomList;


type
  TCustomSyncList = class(TCustomList)
  private
    fLock: TMultiReadExclusiveWriteSynchronizer;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure	Sort; override;
    procedure Clear; override;
    procedure Delete(Item: Pointer); override;
    procedure Add(Data: Pointer); override;
		procedure Insert(Index: Integer; P: Pointer); override;
    function FindItem(Item: Pointer; var Index: Integer): Boolean; override;

    procedure	Lock;
    procedure	UnLock;
    procedure LockW;
    procedure UnLockW;
    function	Count: Integer; override;
  end;

implementation


constructor TCustomSyncList.Create;
begin
  inherited Create;
  fLock := TMultiReadExclusiveWriteSynchronizer.Create;
end;

procedure TCustomSyncList.Delete(Item: Pointer);
begin
  LockW;
  try
		inherited;
  finally
  	UnLockW;
  end;
end;

procedure TCustomSyncList.Add(Data: Pointer);
begin
  LockW;
  try
		inherited;
  finally
  	UnLockW;
  end;
end;

procedure TCustomSyncList.Insert(Index: Integer; P: Pointer);
begin
  LockW;
  try
		inherited;
  finally
  	UnLockW;
  end;
end;

destructor TCustomSyncList.Destroy;
begin
  inherited;
  FreeAndNil(fLock);
end;


function TCustomSyncList.FindItem(Item: Pointer; var Index: Integer): Boolean;
begin
  Lock;
  try
		Result := inherited;
  finally
  	UnLock;
  end;
end;

procedure TCustomSyncList.Sort;
begin
  LockW;
  try
		inherited;
  finally
  	UnLockW;
  end;
end;

procedure TCustomSyncList.Clear;
begin
  LockW;
  try
		inherited;
  finally
  	UnLockW;
  end;
end;

procedure TCustomSyncList.Lock;
begin
	fLock.BeginRead;
end;

procedure TCustomSyncList.UnLock;
begin
	fLock.EndRead;
end;

procedure TCustomSyncList.LockW;
begin
	fLock.BeginWrite;
end;

procedure TCustomSyncList.UnLockW;
begin
	fLock.EndWrite;
end;

function TCustomSyncList.Count: Integer;
begin
  Lock;
  try
    Result := inherited;
  finally
  	UnLock;
  end;
end;

end.
